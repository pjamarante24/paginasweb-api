const router = require("express").Router();
const { ObjectId } = require("mongodb");
const data = require("../data");

router.get("/:collection", async (req, res) => {
  const { collection } = req.params;
  var docs = await data.mongo.find(collection);
  res.json(docs);
});

router.get("/:collection/:id", async (req, res) => {
  const { collection, id } = req.params;
  var docs = await data.mongo.findOne(collection, { _id: ObjectId(id) });
  res.json(docs);
});

router.get("/:collection/id/:id", async (req, res) => {
  const { collection, id } = req.params;
  var docs = await data.mongo.findOne(collection, { codigo: id });
  res.json(docs);
});

router.post("/:collection", async (req, res) => {
  const { collection } = req.params;
  const body = req.body;
  await data.mongo.insertDocument(collection, body);
  res.status(200).send({ message: "Guardado correctamente" });
});

router.put("/:collection/:id", async (req, res) => {
  const { collection, id } = req.params;
  const body = req.body;
  await data.mongo.updateDocument(collection, { _id: ObjectId(id) }, body);
  res.status(200).send({ message: "Guardado correctamente" });
});

router.delete("/:collection/:id", async (req, res) => {
  const { collection, id } = req.params;
  await data.mongo.deleteDocument(collection, { _id: ObjectId(id) });
  res.status(200).send({ message: "Eliminado correctamente" });
});

module.exports = router;
