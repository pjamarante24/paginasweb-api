const Mongo = require("./mongo");

class Data {
  constructor() {
    this.main();
  }

  async main() {
    this.mongo = await new Mongo();
  }

  async tokenExists(token) {
    return await this.mongo.exists("AccessToken", { token: token });
  }

  async auth(username, password) {
    return await this.mongo.exists("usuarios", {
      username: username,
      password: password
    });
  }
}

module.exports = new Data();
