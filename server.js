const express = require("express");
var cors = require("cors");
const uuidv4 = require("uuid/v4");
var bodyParser = require("body-parser");
const routes = require("./routes/routes");
var app = express();
app.use(cors());

var data = require("./data");
var date = new Date();

app.use(bodyParser.urlencoded({ extended: false })); // parse application/x-www-form-urlencoded
app.use(bodyParser.json()); // parse application/json

app.post("/login", async (req, res) => {
  const { username, password } = req.body;
  const auth = await data.auth(username, password);
  if (auth) {
    const token = uuidv4();
    data.mongo.insertDocument("AccessToken", { token: token });
    var user = await data.mongo.findOne("usuarios", { username: username });
    res.json({ token: token, user: user });
  } else res.sendStatus(401);
});

//Authorization
app.all("*", async (req, res, next) => {
  const token = req.headers.authorization;
  if (!token) return res.sendStatus(401);
  const exists = await data.tokenExists(token);
  if (exists) next();
  else return res.sendStatus(401);
});

app.get("/", function(req, res) {
  res.json({ date: date });
});

app.use("/", routes);

var server = app.listen(3000, function() {
  var port = server.address().port;
  console.log("Server listening on port %s.", port);
});
