const MongoClient = require("mongodb").MongoClient;
const assert = require("assert");

function Mongo() {
  var _this = this;

  // Connection URL
  const url = "mongodb://localhost";

  // Database Name
  const dbName = "prestamos";

  var options = {
    useNewUrlParser: true,
    useUnifiedTopology: true
  };

  _this.mongoClient = new MongoClient(url, options);

  return new Promise(function(resolve, reject) {
    _this.mongoClient.connect(function(err, client) {
      assert.equal(err, null);

      console.log("Mongo client successfully connected \n");

      _this.dbConnection = _this.mongoClient.db(dbName);

      resolve(_this);
    });
  });
}

Mongo.prototype.find = function(collectionName, filter) {
  if (!filter) filter = {};
  return new Promise((resolve, reject) => {
    this.dbConnection
      .collection(collectionName)
      .find(filter)
      .toArray(function(err, docs) {
        assert.equal(err, null);
        resolve(docs);
      });
  });
};

Mongo.prototype.findOne = function(collectionName, filter) {
  if (!filter) filter = {};
  return new Promise((resolve, reject) => {
    this.dbConnection
      .collection(collectionName)
      .findOne(filter, function(err, doc) {
        assert.equal(err, null);
        resolve(doc);
      });
  });
};

Mongo.prototype.exists = function(collectionName, filter) {
  var _this = this;
  if (!filter) filter = {};
  return new Promise((resolve, reject) => {
    this.dbConnection
      .collection(collectionName)
      .find(filter)
      .toArray(function(err, docs) {
        assert.equal(err, null);
        resolve(docs.length > 0 ? true : false);
      });
  });
};

Mongo.prototype.findById = function(collectionName, filter) {
  if (!filter) filter = {};
  return new Promise((resolve, reject) => {
    this.dbConnection
      .collection(collectionName)
      .find(filter)
      .toArray(function(err, docs) {
        assert.equal(err, null);
        resolve(docs);
      });
  });
};

Mongo.prototype.insertDocument = function(collectionName, doc) {
  return this.dbConnection.collection(collectionName).insertOne(doc);
};

Mongo.prototype.updateDocument = function(collectionName, doc, updateDocument) {
  delete updateDocument._id;
  return this.dbConnection
    .collection(collectionName)
    .updateOne(doc, { $set: updateDocument }, { upsert: true });
};

Mongo.prototype.deleteDocument = function(collectionName, doc) {
  return this.dbConnection.collection(collectionName).deleteOne(doc);
};

module.exports = Mongo;
